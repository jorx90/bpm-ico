package es.ico.bpm.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import com.everis.alicante.emsa.common.conf.SwaggerConfiguration;
import com.everis.alicante.emsa.common.persistence.config.PersistenceConfig;
import com.everis.alicante.emsa.common.tools.async.AysncConfig;

@SpringBootApplication
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled=true)
@EnableConfigurationProperties
@Import({ PersistenceConfig.class, AysncConfig.class, SwaggerConfiguration.class })
@ComponentScan({ "es.ico.bpm.controller", "com.everis.alicante.emsa" })
public class Application extends ResourceServerConfigurerAdapter {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().httpBasic().disable().authorizeRequests().antMatchers("/**/**").permitAll().anyRequest()
				.authenticated();
		super.configure(http);
	}

}
